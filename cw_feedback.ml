(* Create feedback from CSV file 

#require "core";;
#require "ppx_sexp_conv";;
#require "csv";;

*)

open Core

(* *)
   
let lookup key alist =
  let value = List.Assoc.find alist key ~equal:(String.equal) in
  match value with
  | Some v -> v
  | None -> failwith ("Cannot find key " ^ key)
   
(* Feedback based on numerical intervals *)
         
type num_spec = string * ( (float * string) list) [@@deriving sexp]
let num_spec_col x = match x with c,_ -> c
let num_spec_vals x = match x with _,vs -> vs                                       
              
let rec val_to_feedback vals x =
  match vals with
  | [] -> ""
  | [(_,descr)] -> descr
  | (v,descr)::vs ->
     if Float.compare x v <= 0 then descr
     else val_to_feedback vs x
                                    
let create_numeric_feedback num_spec row =
  let key       =  num_spec_col num_spec in
  let value_str =  lookup key row in
  try 
    val_to_feedback
      (num_spec_vals num_spec)
      (Float.of_string value_str)
    with
      _ -> Printf.printf "Cannot parse %s as float for key %s.\n" value_str key;
           ""

(* Description of output format. *)
                                       
type output_spec_elt =
  S of string   |  (* output string *)
  N of num_spec |  (* map column value to string *)
  C of string   |  (* column value *)
  I of string * string * output_spec_elt  (* conditional statement *)
         [@@deriving sexp]

type output_spec = output_spec_elt list [@@deriving sexp]
                 
let rec feedback_from_spec row spec =
  match spec with
  | (S str) -> str
  | (C col) -> lookup col row
  | (N num_spec) -> create_numeric_feedback num_spec row
  | (I (col,value,spec)) ->
     if String.equal (lookup col row) value then
       feedback_from_spec row spec
     else
       ""

(* Main function to call *)

let data_from_csv csv_file =
  let marks = Csv.load csv_file in
  let header, data = match marks with h :: d -> h, d | [] -> assert false in
  Csv.associate header data
                  
let create_feedback id spec csv_file =
  List.map
    (data_from_csv csv_file)
    ~f:(fun row ->
      ((lookup id row), 
       (String.concat
          (List.map spec ~f:(feedback_from_spec row)))))
                      
(* Parsing sexp expressions for output spec *)
  
let feedback_from_files id csv_file spec_file =
  let mytext = Core.In_channel.read_all spec_file in
  let spec = output_spec_of_sexp (Sexp.of_string mytext) in
  create_feedback id spec csv_file

let csv_file   = ref "marking/marks.csv"
let spec_file  = ref "spec.txt"            
let idcol      = ref "group"
let out_prefix = ref "feedback/group_"
             
let _ =
  Arg.parse
    ["-csv",  Arg.Set_string csv_file,   "CSV file with marks (default " ^ !csv_file ^ ")";
     "-id",   Arg.Set_string idcol,      "Column containing id (default " ^ !idcol ^ ")";
     "-out",  Arg.Set_string out_prefix, "Prefix of output files (default " ^ !out_prefix ^ ")";
     "-spec", Arg.Set_string spec_file,  "Output specification file (default " ^ !spec_file ^ ")"]
    (fun _ -> ())
    "Create feedback messages.";
  List.iter
    (feedback_from_files !idcol !csv_file !spec_file)
    ~f:(fun (id, txt) ->
      Printf.printf "Writing feedback for %s\n" id;
      Core.Out_channel.write_lines (!out_prefix ^ id) [txt])
  
