# README #

Code to produce feedback from coursework marking. It is assumed that
the marks are given in a CSV file. A simple specification language
produces output based on the value of the CSV file. See the examples
directory for a simple example.

### Install ###

    make
    make install

### Usage ###

    cw_feedback -csv example/marks.csv -spec example/spec.txt -out example/feedback/group -id group

### Syntax of output specification ###

    ((S"Hello there group ")
     (C group)
     (S". Your answer to operations is")
     (N
      (operations
       ((0.1"Really bad")
        (0.2"Not so bad")
        (0.5"Quite good")
        (0.9"Really good")
        (1 Perfect))))
     (S "!"))

